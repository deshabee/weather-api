const request = require('request');
const APIKEY = '376a1a6f3e0d8490c840cf7fc6fec1b8';

const getWeather = (LongLat,callback) => {
    request({
        url: `https://api.darksky.net/forecast/${APIKEY}/${LongLat.lat},${LongLat.lng}`,
        json: true
    },(error,response,body)=>{
        if(error){
            callback('unable to connect to forecast.io server');
        }
        else if(response.statusCode === 400){
            callback('unable to fetch weather');
        }
        else if(response.statusCode === 200){
            callback(undefined, `The Current Temp is ${body.currently.temperature} however it feels like ${body.currently.apparentTemperature} , the sky is ${body.currently.summary} and it is a ${body.currently.icon}`);
        }
    });
};

module.exports = {
    getWeather
}
