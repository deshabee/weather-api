const request = require('request');
const APIKEY = 'ODAVWzoWHaEhtbwGMWYXGoKSKc2HHO1e';
const geocodeAddress = (address, callback) => {
    request({
        url: `http://www.mapquestapi.com/geocoding/v1/address?key=${APIKEY}&location=${address}`,
        json: true
    }, (error, response, body) => {
        if (response.statusCode !== 200) {
            callback('Sorry there is sth wrong');
        } else {
            callback(undefined,body.results[0].locations[0].latLng);
        }
    });
}

module.exports = {
    geocodeAddress
}
