const yargs = require('yargs');
const geocode = require('./geocode/geocode');
const weather = require('./weather/weather');

var address;
const argv = yargs.options({
        a: {
            demand: true,
            alias: 'address',
            description: 'Address to fetch weather for',
            string: true
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

address = encodeURIComponent(argv.address);
geocode.geocodeAddress(address, (errorMessage, results) => {
    if (errorMessage) {
        console.error(errorMessage);
    } else {
        console.log();
        // console.log(JSON.stringify(, undefined, 2));
        weather.getWeather(results, (errorMessage, weatherResults) => {
            if (errorMessage) {
                console.error(errorMessage);
            } else {
                console.log(weatherResults);
            }
        });
    }
});